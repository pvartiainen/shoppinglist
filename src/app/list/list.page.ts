import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  itemCount: number;
  items = [];
  newItem: string;

  constructor(private storage: Storage) { }

  ngOnInit() {
    this.storage.get('items').then((val) => {
      this.items = JSON.parse(val);
      if (!this.items) {
        this.items = [];
      }
    });
    this.itemCount = this.items.length;
  }

  addItem(event: any) {
    if (event.keyCode === 13) {
    this.items.push(this.newItem);
    this.newItem = '';
    this.itemCount = this.items.length;
    this.storage.set('items', JSON.stringify(this.items));
    }
  }

  removeItem(index: number) {
    this.items.splice(index, 1);
    this.storage.set('items', JSON.stringify(this.items));
  }

}
